
CC = xelatex
OUTPUT_DIR = .
RESUME_SRCS = $(shell find $(RESUME_DIR) -name '*.tex')

all: resume
	@echo "Done!"

resume:	$(OUTPUT_DIR)/resume.tex $(RESUME_SRCS)
	$(CC) -output-directory=$(OUTPUT_DIR) $<

clean:
	@rm -f *.aux *.dvi *.log *.out *.pdf *.bak
	@echo "Clean done.";
